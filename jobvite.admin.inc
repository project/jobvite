<?php
/**
 * @file
 * Administration pages for Jobvite.
 */

/**
 * Page callback: Jobvite settings.
 *
 * @see jobvite_menu()
 */
function jobvite_config_form() {
  $form = array();
  $form['jobvite_api'] = array(
    '#title' => t('Jobvite API Key'),
    '#type' => 'textfield',
    '#description' => t('Enter the Jobvite api key.'),
    '#default_value' => variable_get('jobvite_api', ''),
  );
  $form['jobvite_sc'] = array(
    '#title' => t('Jobvite Secret'),
    '#type' => 'textfield',
    '#description' => t('Enter the Jobvite Secret key.'),
    '#default_value' => variable_get('jobvite_sc', ''),
  );
  $form['jobvite_company_id'] = array(
    '#title' => t('Jobvite Company ID'),
    '#type' => 'textfield',
    '#description' => t('Enter the Jobvite Company ID.'),
    '#default_value' => variable_get('jobvite_company_id', ''),
  );
  $form['jobvite_career_page_uri'] = array(
    '#title' => t('Jobvite career page uri'),
    '#type' => 'textfield',
    '#description' => t('Jobvite career page uri.'),
    '#default_value' => variable_get('jobvite_career_page_uri', ''),
  );
  $form['jobvite_career_page_title'] = array(
    '#title' => t('Jobvite career page title'),
    '#type' => 'textfield',
    '#description' => t('Jobvite Career page title.'),
    '#default_value' => variable_get('jobvite_career_page_title', ''),
  );
  $form['jobvite_career_page_description'] = array(
    '#title' => t('Jobvite career page description'),
    '#type' => 'textfield',
    '#description' => t('Jobvite Career page description.'),
    '#default_value' => variable_get('jobvite_career_page_description', ''),
  );
  return system_settings_form($form);
}

/**
 * Page callback: Jobvite careers settings.
 *
 * @see jobvite_menu()
 */
function jobvite_careers_config_form() {
  $form = array();
  $form['jobvite_careers_email'] = array(
    '#title' => t('Careers email'),
    '#type' => 'textfield',
    '#description' => t('Enter the career email here.'),
    '#default_value' => variable_get('jobvite_careers_email', ''),
  );
  $instruction = variable_get('jobvite_careers_instruction', array(
    'value' => '',
    'format' => NULL,
  ));
  $form['jobvite_careers_instruction'] = array(
    '#title' => t('Instruction to apply'),
    '#type' => 'text_format',
    '#base_type' => 'textarea',
    '#description' => t('Enter the instruction to apply job.'),
    '#default_value' => $instruction['value'],
    '#format' => 'filtered_html',
  );
  return system_settings_form($form);
}
