For more information about this module please visit:
http://drupal.org/project/jobvite

Contents of this file
---------------------

 * About Drupal
 * About jobvite module
 * Use jobvite
   - Module settings


*** About Drupal
---------------------------------------

Drupal is an open source content management platform supporting a variety of
websites ranging from personal weblogs to large community-driven websites. For
more information, see the Drupal website at http://drupal.org/, and join the
Drupal community at http://drupal.org/community.

Legal information about Drupal:
 * Know your rights when using Drupal:
   See LICENSE.txt in the same directory as this document.
 * Learn about the Drupal trademark and logo policy:
   http://drupal.com/trademark


*** About jobvite module
---------------------------------------

Jobvite API that support Contacts, Candidate, Requisitions and Job feed data
display into webpage.

In order to access the services you will need to be issued an API key and
secret key by the Jobvite
Customer Success team. Please file a request here:
http://recruiting.jobvite.com/support/customer

Live Sites:
1. http://www.kony.com/careers
2. https://www.spotify.com/us/jobs/opportunities

*** Use jobvite
---------------------------------------

- Module settings
  1. Go to admin/config/jobvite/settings for admin configurations.


---------------------------------------
by jagadeesh ramu javvadi
